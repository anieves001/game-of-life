/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */
#include <iostream>
#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
#include <string>
using namespace std;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

int max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
//FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "test"; /* read initial state from here. */

char text[3] = ".O";
// start 

void display(vector<vector<bool> >& board){
// this will display the new board fam. 
for(int i = 0; i < board.size(); i++) 
{ for (int j=0;j<board[i].size(); j++)
	if (board[i][j])
		cout << "O";
	else
		cout << ".";
	cout << endl;
}
}



void parseBoard(vector<vector<bool> >& board, vector<bool>& column){
/*i want to post whats in file to vector in T/F values
 should only be run once each new game*/
//first get the file open bro

FILE* f =fopen(initfilename.c_str(),"rb");
char c;
while(fread(&c,1,1,f) != 0){
	switch (c) {
	case 'O':
		column.push_back(true);
		break;
	case '.':
		column.push_back(false);
		break;
	case '\n':
		board.push_back(column);
		column.clear();
		break;
	
}

}
fclose(f);
}

bool iscellalive(const vector<vector<bool> >& board,const int i,const int j){
//this function will return true if the next cell generation will be alive
int truth= 0; 
int toptest = i-1;
int medtest = i;
int bottest = i + 1;
int columntest = j-1;
//corners
//cout << i <<","<< j<<endl;


for (int topcount=0;topcount<3;topcount++){
	//cout << board.size() << endl;
	int columnmax = j+1;
	if(toptest == -1)
		toptest=board.size() - 1;
	//cout <<"board[" <<toptest<<"][x] is being tested" << endl;
		if (columntest== -1){
			
			int temp = board[i].size() - 1;
			if (board[toptest][temp]==true)
				truth++;
		}
		else if (columntest == board[i].size()){
			if (board[toptest][0]==true)
				truth++;
		}
		else {
			if (board[toptest][columntest]==true)
				truth++;
		}
	//cout <<"board[" <<toptest<<"]["<<columntest<<"] is being tested" << endl;	
	
columntest++;
}

columntest = j-1;
for (int medcount=0;medcount<2;medcount++){
	int medtest = i;
	//cout << board.size() << endl;
	int columnmax = j+1;
	//cout <<"board[" <<toptest<<"][x] is being tested" << endl;
		if (columntest== -1){
			
			int temp = board[i].size() - 1;
			if (board[medtest][temp]==true)
				truth++;
		}
		else if (columntest == board[i].size()){
			if (board[medtest][0]==true)
				truth++;
		}
		else {
			if (board[medtest][columntest]==true)
				truth++;
		}
	//cout <<"board[" <<toptest<<"]["<<columntest<<"] is being tested" << endl;	
//cout << i << "," << columntest << endl;	
columntest= columntest + 2;
}

columntest = j-1;
for (int botcount=0;botcount<3;botcount++){
	//cout << board.size() << endl;
	int columnmax = j+1;
	if(bottest == board.size())
		bottest= 0;
	//cout <<"board[" <<toptest<<"][x] is being tested" << endl;
		if (columntest== -1){
			
			int temp = board[i].size() - 1;
			if (board[bottest][temp]==true)
				truth++;
		}
		else if (columntest == board[i].size()){
			if (board[bottest][0]==true)
				truth++;
		}
		else {
			if (board[bottest][columntest]==true)
				truth++;
		}
	//cout <<"board[" <<toptest<<"]["<<columntest<<"] is being tested" << endl;	
	
columntest++;
} 

// done checking how many neighbors are alive now return true of false
if (board[i][j] == true){ 
	if(truth == 2|| truth == 3)
	return true;
else 
	return false;
}
else { 
	if(truth == 3)
		return true;
	else 
		return false;
}
}

void cellcycle(vector<vector<bool> >& oldboard, vector<vector<bool> >& newboard){
/*the 2 for loops basically go through 0,0 all the way to the end. we do this
 so that we can run the life check for all cells in one function*/
int i,j,count ;
count = 0;

for( i = 0; i < oldboard.size(); i++) 
{ for ( j=0;j<oldboard[i].size(); j++)
		newboard[i][j] =  iscellalive(oldboard,i,j);
}
}


void vectortransfer(vector<vector<bool> >& oldboard, vector<vector<bool> >& newboard){
//start all over again and tranfer the new to the old
for (int i = 0; i<oldboard.size(); i++){
	for(int j=0;j<oldboard[i].size();j++){
		oldboard[i][j] = newboard[i][j];
	
}
}
}


void mainLoop() {
	// create two vecters old and new 
vector<vector<bool> > oldboard;
vector<vector<bool> > newboard;
/*now create a vector that will push the columns into the vector, can be 
used twice */
vector<bool> column;
parseBoard(oldboard,column);
parseBoard(newboard,column);
if (max_gen == 0)
       while (max_gen == 0){
		display(oldboard);
		cout<<endl;
		cellcycle(oldboard,newboard);
		vectortransfer(oldboard,newboard);
       }	      
else if (max_gen != 0) {
		for (int generation=0 ; generation< max_gen;generation++){
		display(oldboard);
		cout<<endl;
		cellcycle(oldboard,newboard);
		vectortransfer(oldboard,newboard);
}

}
}




// finish

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

